package com.intro.pages;

import com.crowdar.bdd.cukes.SharedDriver;
import org.testng.Assert;

public class BlogPage extends PhpTravelBasePage {

    private final String title = "Blog";


    public BlogPage(SharedDriver driver) {
        super(driver);
    }

    public void verifyBlogPageTitle(){
       Assert.assertTrue(getDriver().getTitle().equalsIgnoreCase(title),"Error the title is not the same");
    }
}
