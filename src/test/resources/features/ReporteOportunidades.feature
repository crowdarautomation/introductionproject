Feature: Reporte Oportunidades
  Como Usuario Gerente Comercial
  Quiero acceder al Reporte de Oportunidades y filtrar por un periodo determinado
  Para poder visualizar todas las oportunidades creadas por mi equipo en ese período filtrado

  @ReporteOportunidades @Ignore
  Scenario: Rol Gerente Comercial – Reporte Oportunidades: Filtrar por Periodo
  Given ingreso al sistema con rol Usuario Gerente Comercial
  When  accedo al Reporte de Oportunidades
  And   filtro el reporte por un periodo determinado
  Then  visualizo el reporte de oportunidades
  And   el reporte de oportunidades presenta todas las oportunidades creadas por mi equipo en ese período filtrado

  @ReporteOportunidades @Ignore
  Scenario Outline: Rol Gerente Comercial – Reporte Oportunidades: Filtrar por Periodo <Periodo>
    Given ingreso al sistema con rol <rol>
    When  accedo al Reporte de Oportunidades
    And   filtro el reporte por un periodo <Periodo>
    Then  visualizo el reporte "Reporte de oportunidades"
    And   el reporte de oportunidades presenta todas las oportunidades creadas por mi equipo en ese período <Periodo> filtrado

    Examples:
    |rol                      |Periodo|
    |Usuario Gerente Comercial|Mensual|
    |Usuario Gerente Ventas   |Semanal|


